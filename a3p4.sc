import scala.annotation.tailrec

class Problem3
{
  //Creates List
  def lists(n: Int): List[Int] =
  {
    val r = scala.util.Random
    val L = List.fill(n)(r.nextInt(n))

    println("Original List")

    L.foreach(L => print(L + " "))
    println("")

    L
  }
  //Returns a sorted list
  def InsertSort(theList: List[Int], acc: List[Int] = Nil): List[Int] =
    if (theList.nonEmpty)
    {
      val headVal :: tailVal = theList
    @tailrec //Important
    def insert(headVal: Int, sortedList: List[Int], acc: List[Int] = Nil): List[Int] =
    //actual recursion part
    if (sortedList.nonEmpty) {
      val otherHead :: otherTail = sortedList
      if (headVal <= otherHead) acc ::: headVal :: otherHead :: otherTail
      else insert(headVal,otherTail, acc :+ otherHead)
    }
    else acc ::: List(headVal)
      InsertSort(tailVal, insert(headVal, acc))
    }
    else acc //Base case

}

val num = new Problem3

val sorted = num.InsertSort(num.lists(100))

