class Problem6
{
  def sum(fib: Int): Int = fib match 
  {
    case 0 => 0
    case x if x <= 2 => 1
    case x if x >= 3 => sum(fib-1) + sum(fib-2)
  }
}

val p6 = new Problem6
println(p6.sum(5))