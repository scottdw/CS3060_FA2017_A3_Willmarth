class Problem7
{
  def createMatrix(n: Int): List[List[Int]] =
  {
    val r = scala.util.Random

    val matrix = List.fill(n)(List.fill(n)(r.nextInt(n)))

    matrix
  }
  def product(m1: List[List[Int]], m2: List[List[Int]]): List[List[Int]] =
  {
    m1
  }

  def maltiply[Int](a: List[List[Int]], b: List[List[Int]])(implicit n: Numeric[Int]) = {
    import n._
    for (row <- a)
      yield for(col <- b.transpose) // make it easier to multiply and less frustrating
        yield row zip col map Function.tupled(_*_) reduceLeft (_+_)
        //zips the row for the index, maps with col, defines how tomultiply the rows and columns then adds
  }

}

val p7 = new Problem7


val num = 3

val m1 = p7.createMatrix(num)
println("Matrix 1 " + m1)
val m2 = p7.createMatrix(num)
println("Matrix 2 " + m2)
val m3 = (p7.maltiply(m1, m2))
println("The product " + m3)