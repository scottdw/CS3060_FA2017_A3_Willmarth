class Problem3
{
  def lists(n: Int)
  {
    val r = scala.util.Random
    val L = List.fill(n)(r.nextInt(n))

    println("Original List")

    L.foreach(L => print(L + " "))
    println("")
    println("Sorted List")

    val S = L.sortWith(_ < _)
    S.foreach(S => print(S + " "))
    println("")
    println("Reverse List")

    val R = L.sortWith(_ > _)
    R.foreach(R => print(R + " "))
    println("")
    println("Odd Index Only")

    val O = Iterator.from(1, 2).takeWhile(_ < R.size).map(R(_))
    O.foreach(O => print(O + " "))
    println("")
  }

}

val num = new Problem3

num.lists(100)
