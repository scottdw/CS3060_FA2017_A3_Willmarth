import scala.Boolean

class Shape(val color: String, val filled: Boolean)
{
  val color = "red"
  val filled = true

  def this() = this("", false);
  def this(color: String, filled: Boolean) = this(color, filled);

  def getColor(): String = color
  def isFilled(): Boolean = filled

  def setColor(color: String = color) = new Shape(color, filled)
  def setFilled(filled: Boolean = filled) = new Shape(color, filled)
}


class Circle(override val color: String, val filled: Boolean, val radius: Double) extends Shape(color, filled)
{
  val color = "red"
  val filled = true
  val radius = 1.0

  def this() = this("", false, 0.0);
  def this(radius: Double) = this("", false, radius)
  def this(color: String, filled: Boolean, radius: Double) = this(color, filled, radius);

  def getRadius(): Double = radius;
  def getArea(): Double = radius * radius * 3.14
  def getPerimeter(): Double = 2 * radius * 3.14

  def setRadius(radius: Double = radius) = new Circle(color, filled, radius)
}

class Rectangle(override val color: String, val filled: Boolean, val width: Double, val length: Double) extends Shape(color, filled)
{
  val color = "red"
  val filled = true
  val width = 1.0
  val length = 1.0

  def this() = this("", false, 0.0, 0.0);
  def this(width: Double, length: Double) = this("", false, width, length)
  def this(color: String, filled: Boolean, radius: Boolean, width: Double, length: Double) = this(color, filled, width, length);

  def getWidth(): Double = width;
  def getLength(): Double = length;
  def getArea(): Double = width * length
  def getPerimeter(): Double = (2 * width) + (length * 2)

  def setWidth(width: Double = width) = new Rectangle(color, filled, width, length)
  def setLength(length: Double = length) = new Rectangle(color, filled, width, length)
}

class Squaree(override color: String, filled: Boolean, side: Double) extends Rectangle
{
  val color = "red"
  val filled = true
  val side = 1.0

  def this() = this("", false, 0.0);
  def this(side: Double) = this("", false, side)
  def this(color: String, filled: Boolean, radius: Boolean, side: Double) = this(color, filled, side);

  def getside(): Double = side;
  def getArea(): Double = side * side
  def getPerimeter(): Double = 4 * side

  def setSide(side: Double = side) = new Rectangle(color, filled, side)
}