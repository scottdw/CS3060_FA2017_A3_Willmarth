import scala.io.Source
import scalax.io._
import java.io.{File, PrintWriter}
import java.io
import java.util.regex.Pattern

class Problem8
{
  def output(line: String): String =
  {
    val pattern = Pattern.compile(", *") //Looks for split
    val matcher = pattern.matcher(line)
    if (matcher.find) {
      val string1 = line.substring(0, matcher.start)
      val string2 = line.substring(matcher.end)
      val op = string1 + ":" + string2 + ":" + distance(line)
      return op
  }
  def distance(line: String): Int =
  {
    val pattern = Pattern.compile(", *") //Looks for split
    val matcher = pattern.matcher(line)
    if (matcher.find) {
      val string1 = line.substring(0, matcher.start)
      val string2 = line.substring(matcher.end)
      val size1 = string1.length
      val size2 = string2.length
      val dif = math.abs(size1 - size2)
      if(size1 > size2)
        {
          return dif + compare(string2.toList, string1.toList)
        }
      else
        {
          return dif + compare(string1.toList, string2.toList)
        }
    }
    0
  }

  def compare(a: List[Char], b: List[Char]): Int =
  {
    if(a.isEmpty) 0
    else
      {
        if(a(0) == b(0))
        {
          compare(a.tail, b.tail)
        }
        else
          {
            1 + compare(a.tail, b.tail)
          }
      }
  }

}

val p8 = new Problem8
val writer:Output = Resource.fromFile("output.txt")

Source

  .fromFile("input.txt")
  .getLines
  .map { line => writer.write(p8.output(line))(Codex.UTF8)
  }
  .foreach(println)
