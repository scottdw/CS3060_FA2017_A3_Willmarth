class Problem5
{
  def sum(fib: Int): Int =
  {
    if(fib == 0)
      {
        0
      }
    else
      {
        if(fib <= 2)
          {
            1
          }
        else
          {
            sum(fib-1) + sum(fib-2)
          }
      }

  }
}

val p5 = new Problem5

println("Fibonacci 0 -> " + p5.sum(0))
println("Fibonacci 1 -> " + p5.sum(1))
println("Fibonacci 2 -> " + p5.sum(2))
println("Fibonacci 6 -> " + p5.sum(6))
println("Fibonacci 30 -> " + p5.sum(30))