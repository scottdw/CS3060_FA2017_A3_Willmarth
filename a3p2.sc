class Problem2
{
  def theWord(word: String, sub: String, index: Int): Int = {
    val len = sub.length()
    if(len > word.length)
      {
        println("Substring too big")
        -1
      }
    else
      {
        if(index + (len-1) < word.length())
        {
          if (sub == word.substring(index, index + len)) {
            println("Index was found")
          }
          else {
            return theWord(word, sub, index + 1)
          }

          return index
        }
        else
        {
          println("Index was not found")
          return -1
        }
      }
  }
}

val index = 0
val p2 = new Problem2
println(p2.theWord("human", "man", index))

println(p2.theWord("the American way", "can", index))

println(p2.theWord("someBODY", "once told me", index))

println(p2.theWord("Autumn", "Fall", index))